# Huddle

Huddle ist die Meetingplatform für BundesMessenger für Audio-, Videoanrufe und
Besprechungen innerhalb von Teams, die immer sicher sind.

## Das ist die Idee hinter Huddle

In der analogen Welt ist ein Huddle ein kleiner Besprechungsraum der spontan
genutzt kann, jedoch nicht im Vorfeld gebucht werden muss. Genau das soll Huddle
in BundesMessenger darstellen - Eine moderne Art sich adhoc und remote treffen
und einfach zusammenzuarbeiten.

**Huddles** im BundesMessenger…

- werden **ad hoc** und ungeplant gestartet
- sind super sicher durch **permanente Ende zu Ende Verschlüsselung**
- können direkt aus einer **Direktnachricht** oder einem **Raum** im Messenger
  gestartet werden
- alle anderen Mitglieder in der Direktnachricht oder im Raum werden über den
  Start des Huddles informiert und können **spontan beitreten** – müssen sie
  aber nicht
- sind für **maximal 30 Personen** gleichzeitig nutzbar
- bieten **Audio**, **Video** und **Bildschirmfreigabe**
- es können auch **mehrere Bildschirme gleichzeitig** freigegeben werden und
  sind für alle im Raum sichtbar
- Teilnehmervideos & Bildschirmfreigaben können **individuell angepasst** werden
- gibt es in Zukunft für **alle Plattformen** (Android, iOS, Web).
- eine Möglichkeit zur telefonischen Einwahl wird es niemals geben aber eine
  Gastteilnahme via App
- alle Teilnehmenden benötigen dafür nur die BundesMessenger App
- Informationen aus dem Huddle bleiben auch danach im Chatverlauf erhalten

## Das ist der Mehrwert

Huddles im BwMessenger und BundesMessenger bieten **spontanen, kreativen
Austausch** in Teams, ohne vorab ein Online-Meeting zu planen. Einfach ad hoc
mit **einem Klick** einen Huddle öffnen und starten. **Tipp:** im Huddle kann
auch mehr als ein Bildschirm gleichzeitig geteilt werden – einfach mal
ausprobieren.

## Huddle oder Online-Meeting – das sind die Unterschiede

Beide Setups ergänzen sich und haben unterschiedliche Einsatzgebiete und
Anwendungsfälle.

| Huddle im BundesMessenger                      | andere Online-Meetings                     |
| ---------------------------------------------- | ------------------------------------------ |
| - Spontan                                      | - Geplant                                  |
| - Einfach – nur ein Klick zum Start            | - Inhalte bleiben verfügbar                |
| - Direkt aus einer Direktnachricht oder Raum   | - Vorab organisiert und versendet          |
| - Maximal 30 Teilnehmer                        | - Via Outlook oder Meeting-Tool            |
| - Auf dem PC und bald auch auf dem Smartphone  | - Viele Teilnehmende möglich               |
| - Keine Telefoneinwahl                         | - Auf PC und SmartPhone                    |
| - Freigabe von mehreren Bildschirmen           | - Einwahl per Telefon möglich              |
| - Nur für BwMessenger und BundesMessenger User | - Nur eine Bildschirmfreigabe gleichzeitig |
| - Inhalte bleiben verfügbar                    | - Für interne und externe Teilnehmende     |
| - Audio, Video und Metadaten E2EE              | - Nur teilweise oder gar nicht E2EE        |

## Zeitplan

**Sommer 2024:** Huddle Beta mit SFU für Web

**Herbst 2024:** Huddle Beta in den mobilen Apps auf Basis BuM X

**Winter 2024:** Sichere Teilnahme für Gäste

## Technologie

Huddle nutzt als Basis [Element Call](https://call.element.io) und die
[Livekit SFU](https://github.com/livekit/livekit).

In Zukunft wird es Huddle auch als eigenständiges Produkt geben. D.h. Du
benötigst keinen BundesMessenger um Dich online zu treffen.
