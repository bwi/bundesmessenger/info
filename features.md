# BundesMessenger Featureübersicht (April 2023)

## Vorhanden

Folgende Funktionen sind bereits im BundesMessenger vorhanden.

| Funktion                                           | Android | iOS | Web |
| -------------------------------------------------- | ------- | --- | --- |
| Zugriffsschutz mit Pin und Biometrie               | ✔       | ✔   | ✘   |
| Anzeige im hellen und dunklen Design               | ✔       | ✔   | ✔   |
| Nutzung mit mehreren Endgeräten                    | ✔       | ✔   | ✔   |
| Direktnachrichten                                  | ✔       | ✔   | ✔   |
| Öffentliche und private Räume                      | ✔       | ✔   | ✔   |
| Textnachrichten                                    | ✔       | ✔   | ✔   |
| Textformatierung mit Markdown                      | ✔       | ✔   | ✔   |
| Sprachnachrichten                                  | ✔       | ✔   | ✔   |
| Emojis                                             | ✔       | ✔   | ✔   |
| Dateiaustausch                                     | ✔       | ✔   | ✔   |
| Bilder und Videos                                  | ✔       | ✔   | ✔   |
| Push Benachrichtigungen                            | ✔       | ✔   | ✔   |
| Personensuche                                      | ✔       | ✔   | ✔   |
| Raumverzeichnis für öffentliche Räume              | ✔       | ✔   | ✔   |
| Chats als Favoriten markieren                      | ✔       | ✔   | ✔   |
| Profilbilder für Personen und Räume                | ✔       | ✔   | ✔   |
| Offene und versteckte Umfragen                     | ✔       | ✔   | ✔   |
| Funktionsbezeichnungen für Raummitglieder vergeben | ✔       | ✔   | ✔   |
| Benachrichtigungszeiten konfigurieren              | ✔       | ✘   | ✘   |
| Benachrichtigungen pro Chat ein- und ausschalten   | ✔       | ✔   | ✔   |
| Rollen- und Rechtemanagement für Raummitglieder    | ✔       | ✔   | ✔   |
| Chat-Bubble-Optik                                  | ✔       | ✔   | ✔   |
| Ankündigung von neuen Funktionen                   | ✔       | ✔   | ✔   |

## In Vorbereitung

Folgende Funktionen sind bereits im BwMessenger vorhanden. Wir arbeiten an einer
Portierung in den BundesMessenger.

| Funktion                                             | Android | iOS | Web |
| ---------------------------------------------------- | ------- | --- | --- |
| Standortfreigabe                                     | ✔       | ✔   | ✔   |
| Teilen von Personen, Räumen und Nachrichten als Link | ✔       | ✔   | ✔   |
| Huddle Meetings                                      | ✘       | ✘   | ✔   |
