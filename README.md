<!-- markdownlint-disable -->
<div align="center">
  <img src="images/bum_next.png" alt="BundesMessenger X logo" width="258" height="258">
</div>

<div align="center">
  <h2 align="center">BundesMessenger</h2>
</div>
<div align="center">
  Souveränität und Sicherheit und Freiheit
</div>
<!-- markdownlint-enable -->
<!-- markdownlint-disable MD026 MD036 -->

# BundesMessenger

## MLS MSC UPDATE

👋 Moin zusammen!

Heute ist es endlich soweit – wir haben
[MSC 4256](https://github.com/matrix-org/matrix-spec-proposals/pull/4256) für
Matrix veröffentlicht! Damit gehen wir den ersten großen Schritt, dezentrale
Infrastruktur mit moderner Verschlüsselung zu kombinieren. Das Ziel? Eine noch
sicherere, effizientere und skalierbare Kommunikation 💬 – offen und frei für
alle.

❓Warum das Ganze?

- Wir möchten das Beste aus beiden Welten zusammenbringen 🌍 – die Flexibilität
  von Matrix und die State-of-the-Art-Sicherheit von Message Layer Security
  (MLS). Neben der Sicherheit ist ein besonderes Ziel, die Kommunikation in
  großen Gruppen zu verbessern – und dabei den Endgegner "unable to decrypt" 🥷
  endlich zu besiegen.

- Grundlage für mehr: MLS ist eine wichtige Grundlage, um den BundesMessenger
  für VS-NfD-Kommunikation 🛡️ vorzubereiten. Das hier ist nur ein erster
  Schritt, aber ein sehr bedeutender.

💌 Shoutout an Element & unsere Partner Element war mit seinen Vorschlägen zur
Nutzung von MLS ([dMLS](https://gitlab.matrix.org/uhoreg/matrix-dmls) &
[Linearized Matrix](https://datatracker.ietf.org/doc/draft-ralston-mimi-linearized-matrix/))
eine Inspiration 💡 für uns. Leider passten diese Ansätze nicht zu 100 % zu
unseren spezifischen Anforderungen im Messenger. Aber genau das hat uns
motiviert, selbst aktiv zu werden 💪 – mit [Cryspen](https://cryspen.com) und
[Famedly](https://famedly.com) an unserer Seite. Ein riesiges Dankeschön an -
Franziskus, Karthik, Jan, Niklas & Nicolas – ihr habt Matrix und MLS wirklich
durchgespielt! 🎮

Wir freuen uns auf die weitere Zusammenarbeit mit der Matrix Foundation und der
Community auf Basis von
[MSC 4244](https://github.com/matrix-org/matrix-spec-proposals/blob/travis/msc/mls/00-core/proposals/4244-rfc9420-mls-for-matrix.md)
sowie [MSC 4256](https://github.com/matrix-org/matrix-spec-proposals/pull/4256),
um gemeinsam das Beste 💯 für die gesamte Matrix-Welt zu schaffen.

🛠️ So jetzt ihr!

- MSC checken: Schaut euch
  [MSC 4256](https://github.com/matrix-org/matrix-spec-proposals/pull/4256) an
  und bringt euch ein 📢.
- Feedback geben: Wir wollen alles hören – was gut läuft 👍, was hakt ❌ und was
  wir noch besser machen können 🛠️.
- Nerd 🤓 sein: Schnappt euch den
  [Code](https://gitlab.opencode.de/bwi/bundesmessenger/mls) auf OpenCode und
  versucht es selbst. Der Demo Client ist wirklich wunderschön geworden 🥰

🎉 Lasst die Party starten! Wir freuen uns darauf, gemeinsam 🤝 und offen an
diesem Thema weiterzuarbeiten, um die besten Ergebnisse zu erzielen.

Und jetzt ran ans testen 🧪! Wir sind gespannt auf eure Meinungen und Ideen .
Das hier ist erst der Anfang einer spannenden Reise 🚀 zu mehr sicherer und
souveräner Kommunikation.

## ☃❄🏂 WINTER UPDATE 2024 🏂❄☃

Hey Community! Der Winter ist da und wir melden uns auch nochmal zurück. Schnee
fällt zwar noch nicht wirklich, aber wir bringen euch coole Updates. 🥶 Lasst
uns reinschauen, was es Neues gibt:

## 📱 Endlich ist es soweit die Beta der neuen Apps startet! 🎁 🎉

Ein neues Zeitalter für unsere mobilen Apps bricht an! Frisch, funktional und
fit für die Zukunft. Um die Benutzererfahrung zu verbessern, neue Funktionen
bereitzustellen und aktuelle technologische Standards zu integrieren, haben wir
die Apps von Grund auf neu entwickelt. Lade jetzt die Beta-Version herunter und
werde Teil der Entwicklung von **BundesMessengerX**. Deine Meinung zählt!

<!-- markdownlint-disable -->
<p align="center">  
  <a href="https://play.google.com/store/apps/details?id=de.bwi.messenger.x">
  <img alt="Download Google Play Store" src="images/google-play-badge.png" width=160>
  </a>
</p>

<p align="center">  
  <a href="https://apps.apple.com/de/app/bundesmessenger-x/id6738500048">
  <img alt="Download Apple App Store" src="images/appstore_apple.svg" width=160>
  </a>
</p>
<!-- markdownlint-enable -->

Ihr könnt die bisherige BundesMessenger App und BundesMessengerX _parallel_
benutzen! Einige Funktionen sind im BundesMessengerX noch in der Mache, aber
keine Sorge – wir arbeiten hart daran, sie euch in den nächsten Updates zur
Verfügung zu stellen. Als nächstes kommt z. B. Benachrichtigungszeiten,
Sitzungen verwalten und vieles mehr.

Wir planen bis Sommer 2025 die Betaphase zu beenden und im Anschluss werden die
"alten" Clients ausgephast. 🤞

Um die neuen Clients weiter zu verbessern brauchen wir euer Feedback. Schaut
euch bitte erst einmal die neuen Apps an. Mit einem der nächsten Updates werden
wir euch die Möglichkeit geben aus den Beta Apps eure Eindrücke, Probleme,
Verbesserungen an uns zu übermitteln, damit wir diese für die Weiterentwicklung
nutzen können. 💪

Aufgrund der Abhängigkeit der BundesMessenger Apps zu element, unterliegen diese
nun auch der [AGPLv3](#agplv3). Ist weiterhin kein Problem. Details dazu auf dem
Link.

## 🚀 Bereit für Meetings? – Huddle Beta startet 📹🎙🎭

Ob schnelle Absprache oder tiefgründiges Brainstorming, der BundesMessenger wird
bald mit ersten Funktionen für sichere Meetings ausgestattet.

Dazu bietet unser
[Helm Chart](https://gitlab.opencode.de/bwi/bundesmessenger/backend/helm-chart)
die Option Huddle zu
[aktivieren](https://gitlab.opencode.de/bwi/bundesmessenger/backend/helm-chart/-/blob/main/docs/livekit_server.md).
Weiterhin muss eine zusätzliche Kernkomponente (Livekit SFU) im Backend
bereitgestellt werden, welche die Schnittstelle für die Echtzeitkommunikation
ist. Das Beste ist: Auch hier wird auf eine Ende-zu-Ende Verschlüsselung
aufgesetzt und damit sind auch Meetings durchgängig verschlüsselt, wie ihr es
bei BundesMessenger gewohnt seid! 🔒 Unsere Backend Ingenieure 🤓 haben viel
dafür gemacht, damit es so einfach wie möglich von der Hand geht.

Sobald ihr die SFU bereitgestellt habt und das Helm Chart entsprechend
konfiguriert und deployed ist, könnt ihr direkt mit der Huddle-Beta im Web
Client starten. Das ist erst der Anfang 😄 Die neuen BundesMessengerX Apps
werden mit zukünftigen Updates auch die Huddle-Beta unterstützen 📹 Damit ist
die Grundlage für sichere Meetings auch auf mobilen Endgeräten gelegt und ihr
könnt euch danach auf weitere Features freuen.

Her mit eurem Feedback!

## Messaging Layer Security (MLS)

Wir arbeiten ziemlich aktiv daran, MLS auf die Bedürfnisse der Bundeswehr
anzupassen. Heißt wir versuchen die Vorteile von Matrix und MLS zu vereinen, was
aus unserer Sicht einige Mehrwerte hat:

- Performanterer Austausch in sehr großen (50k+) Ende-zu-Ende verschlüsselten
  Räumen
- Verifizierte Identitäten im Chat
- Die drei großen P's: **Perfect-Forward-Secrecy (PFS)**,
  **Post-Compromise-Security (PCS)** und
  **[Post-Quanten-Resistance](https://cryspen.com/post/pq-mls/) (PQR)**
- Nie wieder Unable-To-Decrypt 😜

MLS wurde bisher nur bei zentralen Systemen eingesetzt. Matrix wiederum steht
für einen dezentralen Ansatz. Wir brauchen beides! 🍸 Dafür werden wir Anfang
2025 einen Matrix Specification Change "MLS on Matrix" 🤓 dazu veröffentlichen,
um einen großen Schritt in diese Richtung zu machen. Auch von dieser Entwicklung
sollt ihr im BundesMessenger uneingeschränkt profitieren, weil uns eure
Sicherheit einfach am Herzen liegt. 💝 Wir lassen euch wissen wann, wie und wo
es weitergeht. 🤞

## 🪖 Federation Testing mit der Bundeswehr und BWI

Wir freuen uns, dass die **Bundeswehr** und die **BWI** die Föderation gerade
auf Herz und Nieren testen. Seid auch ihr dabei und testet die Föderation!
Unsere Vision einer vernetzten, sicheren Kommunikation wird Realität – mit eurer
Hilfe! Wie ist es mit euch? Föderiert ihr schon? Wie sind eure Erfahrungen?

### Föderation - Meldet euch

BundesMessenger alleine ist schön, zusammen ist es noch viel besser.

Damit wir ein gemeinsames Netzwerk aufbauen können, ist es wichtig, dass ihr
euch gegenseitig finden könnt. Dazu arbeiten wir an einen Adressbuch um
Föderationspartner zu finden. Im ersten Schritt soll es dazu ein Verzeichnis
geben. Wir halten auf dem Laufenden wie das genaue Vorgehen dazu ist.

Zukünftig soll es eine integrierte Verwaltung für Föderationen im Admin Portal
geben, um schnell und einfach mit anderen Nutzerhäusern in Kontakt zu treten.
Wir gehen das Schritt für Schritt in 2025 an.

## VS-NfD und der BundesMessenger 🧙‍♀️

VS-NfD ist ein nicht ganz einfaches Thema 🕵 aber extrem wichtig für die
Sicherheit und somit für den BundesMessenger. Um eine Freigabeempfehlung zu
erhalten, ist das BSI zu involvieren. Dieses möchten wir für den BundesMessenger
angehen und euch zur Verfügung stellen. Auch hier sind wir bereits am Ball 🎱

Im ersten Schritt möchten wir die Apps auf Basis Apple Indigo und Samsung Knox
Native in die SecOps Liste für die Lösungen beim BSI aufnehmen lassen. Damit
kannst Du den BundesMessenger als VS-NfD App in DEINEM Netzwerk einsetzen. Für
das volle VS-NfD Paket, welches alle Anforderungen an einen sicheren,
föderierten Messenger des BSI erfüllt, werden wir allerdings noch mehr Zeit
benötigen, da dieses die vollständige Umsetzung z.B. von MLS voraussetzt. Auch
Huddle haben wir hierbei im Blick.

TL;DR: Die Zukunft sieht sicher aus.

## 🛠 Deployment-Modelle für jeden Bedarf

Egal ob on-premise, als IaaS oder SaaS über die T-Systems – der BundesMessenger
ist vielseitig betreibbar! Wusstet ihr, dass die Konzessionsgebühren an die BWI
direkt in die Weiterentwicklung des BundesMessengers fließen? 💰 Damit hilft
jeder Nutzer dabei, dass das Produkt für die öffentliche Verwaltung
weiterentwickelt wird und weiterhin in neue Funktionen und höchste
Sicherheitsstandards investiert werden kann.

## 🤓 Warum der BundesMessenger zum "knutschen" ist 😘

Keep it Simple & Secure (KISS) – das ist ein Motto für uns! 💡 Wir setzen auf
Übersichtlichkeit und eine intuitive Bedienung für jeden, der den Messenger
einsetzt, ohne dabei Kompromisse bei der Sicherheit einzugehen. Andere mögen
eine Vielzahl an Features anhäufen, die nicht wirklich auf das berufliche Umfeld
abgestimmt sind – wir halten es einfach und sicher. Genau so, wie es sein
sollte! Immer nach dem Motto _Safety First_

Und das ist auch das, was uns von anderen unterscheidet neben der großartigen
Community. Bei uns gilt: Alles unter 80% ist nicht genug. Wir arbeiten
unermüdlich daran, euch den besten Messenger im Umfeld der öffentlichen
Verwaltung zur Verfügung zu stellen. Also keine halben Sachen – nur das Beste
für euch!

## 🌐 Unsere neuen Domains bundesmessenger.de & bundesmessenger com

Trommelwirbel, bitte! 🥁 Die Domains
[bundesmessenger.de](https://bundesmessenger.de)  
und [bundesmessenger.com](https://bundesmessenger.com) gehören jetzt uns. Damit
habt ihr zukünftig einen zentralen Anlaufpunkt für alle Infos rund um euren
Messenger. Bald wird es dort eine eigene neue Präsenz geben, randvoll mit Infos,
Updates und einer 🚀 Roadmap, damit ihr immer einen vollständigen Überblick habt
und wisst was ihr erwarten könnt.

Stay tuned - again!

## 🏄 Update der Dokus für Endnutzer:innen

Weil ihr hoffentlich gerade auf der bundesmessenger.de & .com gewesen seid,
schaut doch noch kur auf
[docs.bundesmessenger.info](https://docs.bundesmessenger.info) vorbei. Wir haben
die Navigation nochmal komplett überarbeitet, einige Inhalte aktualisiert und
eine Dokumentation für Föderationen hinterlegt 📙

Vielleicht ist was für euch dabei oder ihr könnt noch was dazulernen. 🎓
Zukünftig wird die Seite noch barrierefreier sein (z. B. Navigation mit der
Tastatur) und eine Offline-Funktion realisiert werden.

Wusstet ihr eigentlich, dass es auch eine Version für die Admins unter euch
gibt? Hier nochmal der Link :
[docs.bundesmessenger.dev](https://docs.bundesmessenger.dev)

In beiden Learning Portalen wird es in Zukunft größere und viele neue Inhalte
geben. Also schaut regelmäßig rein. ⭐

## 🌟🌟🌟 App Stores

Wenn euch unsere Apps gefallen, würden wir uns total über eine Rückmeldung mit
vielen Sternen (egal ob neu oder alt) im Apple AppStore und Google Playstore
freuen. Euer positives Feedback bedeutet uns hier sehr viel. 🎁🎄🎅

**Vielen Dank, dass ihr Teil dieser Reise seid!**

## ☃ ❄ 🏂 WINTER UPDATE ENDE 🏂 ❄ ☃

Der BundesMessenger ist ein sicherer Messenger (Kommunikationslösung) für die
öffentliche Verwaltung (ÖV). Der Unterschied zu anderen Messengern ist, dass die
Kontrolle der Daten in Deiner Hand bleibt und nicht in der öffentlichen Cloud
abgelegt, gesichert und verarbeitet wird.

**Einfach 100% digital souverän**.

Der BundesMessenger ist der Messenger, der auf die Bedürfnisse der ÖV angepasst
und optimiert ist und stetig für diesen Einsatz weiterentwickelt wird. Von und
für die Behörden in Deutschland.

Das Ziel ist eine moderne & sichere Zusammenarbeitsplattform auf Open Source
Basis für Deutschland zu schaffen, die sich den Bedürfnissen und Vorgaben der
Deutschen Verwaltung anpasst.

Kann das funktionieren?

> Der BwMessenger der Bundeswehr hat bewiesen, dass das funktionieren kann.
> Hinter dem BundesMessenger stecken die gleichen Köpfe wie hinter dem
> BwMessenger. Er hat die gleiche DNA wie der BwMessenger.

powered by [BWI GmbH](https://bwi.de)

## Inhaltsverzeichnis

- [Projektübersicht](#projekt%C3%BCbersicht)
  - [Über](#%C3%BCber)
  - [Ziele](#ziele)
  - [Die Idee der Zusammenarbeit heißt Föderation](#die-idee-der-zusammenarbeit-hei%C3%9Ft-f%C3%B6deration)
  - [Ihr wollt einen eigenen Messenger?](#ihr-wollt-einen-eigenen-messenger)
  - [Apps](#apps)
  - [Backend](#backend)
  - [AGPLv3](#agplv3)
- [Roadmap](#roadmap)
- [Guidelines](#guidelines)
- [Mitarbeit](#mitarbeit)
- [Dokumentation](#dokumentation)
  <!-- markdownlint-disable-next-line MD051 -->
- [Features & Vorteile](#features-vorteile)
- [FAQ](#faq)
- [Support](#support)
  <!-- markdownlint-disable-next-line MD051 -->
- [Bugs & Feature Requests](#bugs-feature-requests)
- [Rechtliches](#rechtliches)
  - [Copyright](#copyright)

# Projektübersicht

## Über

Die Grundlage für dieses Projekt ist der BwMessenger der Bundeswehr. Dieser
wurde von der BWI für die Bundeswehr im Jahr 2018 ins Leben gerufen. Die
Grundlage hierfür ist das [Matrix Protokoll](https://spec.matrix.org/latest/)
und die _element_ Clients sowie die Referenzimplementierungen für den
Applikationsserver und Push Gateway.

Auf dieser Grundlage hat die BWI für die Bundeswehr einen angepassten und
skalierbaren Messenger entwickelt, der den IT Sicherheitsvorgaben der Bundeswehr
entspricht.

## Ziele

- sicherer und offener Standard für alle
- einheitliche Lösung für alle
- übergreifende Kommunikation für alle
- einfacher Einstieg für alle
- schnelle Verfügbarkeit und Verbreitung

Das Ziel ist es einen sicheren Kommunikationsverbund für Deutschland zu
implementieren, der auf dem Open Source Standard Matrix aufbaut und für die
Verbindung der Systeme sog. Föderationen nutzt. Die App verweist anhand von
deinem Benutzernamen auf das dazugehörige Backend.

Im Rahmen des Projektes werden mehrere Komponenten entwickelt:

- Core - Alle Server Module für den BundesMessenger
- Apps - Apps für den Zugriff von mobilen Endgeräten und aus dem Browser
- AddOns, u.a.:
  - Admin Portal - für die einfache Verwaltung und Anpassungen

Die Pflege der Apps übernehmen wir. So kannst Du bequem, die App einfach aus dem
App Store herunterladen und musst Dich nicht mit dem Bereitstellungsprozess,
Programmierung, Pflege, Updates von Apps beschäftigen und kannst Dich aufs
chatten konzentrieren.

## Die Idee der Zusammenarbeit heißt Föderation

<!-- markdownlint-disable -->
<div align="center">
  <img src="images/matrix-infrastruktur.svg" alt="Überblick über Föderation" width="755" height="360">
</div>
<!-- markdownlint-enable -->

Wenn es soweit ist, hierzu viel mehr. Dieses wird ebenfalls ein AddOn werden,
welches hier zur Verfügung gestellt wird.

## Ihr wollt einen eigenen Messenger?

Für den Austausch zum BundesMessenger haben wir einen
[Matrix Raum](https://matrix.to/#/#opencodebum:matrix.org) erstellt.

<!-- markdownlint-disable -->
<div align="center">
  <img src="images/qr_matrix_room.png" alt="QR Code Matrix">
</div>
<!-- markdownlint-enable -->

Kein Matrix Client zur Hand, dann auch gerne über unser
[Email Postfach](mailto:bundesmessenger@bwi.de).

Wir freuen uns auf den Austausch.

## Apps

Die Apps sind in den Stores von Apple und Google verfügbar. Den Quellcode zu den
Apps findest Du unter
[BundesMessenger Clients](https://gitlab.opencode.de/bwi/bundesmessenger/clients).

<!-- markdownlint-disable -->
<div align="center">
  <a href=https://play.google.com/store/apps/details?id=de.bwi.messenger>
  <img alt="Download Google Play Store" src="https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-android/-/raw/master/resources/img/google-play-badge.png" width=160>
  </a>
</div>

<div align="center">
  <a href=https://apps.apple.com/de/app/bundesmessenger/id1616866351>
  <img alt="Download Apple App Store" src="images/appstore_apple.svg" width=160>
  </a>
</div>
<!-- markdownlint-enable -->

**Die Nutzung des eigenen Backends mit den Apps aus den Stores setzt die
Freischaltung Deiner Domain in den Apps voraus. Du kannst Deine Freischaltung
[hier anfragen](https://messenger.bwi.de/ich-will-bum).**

Es gibt einen Demonstrator für alle diejenigen aus der öffentlichen Verwaltung,
die nicht die Möglichkeit haben ein eigenes Backend zur Erprobung aufzubauen.
Bitte richtet eure Anfrage dazu über die
[Produkt Webseite](https://messenger.bwi.de/ich-will-bum) an uns. Wir freuen uns
Dir den BundesMessenger zeigen zu können.

## Backend

Alle Bestandteile des Backends für den eigenen Betrieb findest Du unter
[BundesMessenger Backend](https://gitlab.opencode.de/bwi/bundesmessenger/backend).
Die BWI stellt Dir keine Instanz zur Verfügung. Der Betrieb Deiner Instanz muss
durch Dich oder einen Dienstleister erbracht werden.

Du benötigst auf jeden Fall einen Kubernetes Cluster (K8S) und Helm um das
BundesMessenger zu betreiben. Wenn Du keins hast, fang jetzt an!

BundesMessenger ist eine Gesamtlösung. Es ist nicht beabsichtigt ein beliebiges
Matrix Backend für den Betrieb mit den BundesMessenger Apps zu verwenden.

- On Premise
- IaaS
- SaaS

Basis für das SaaS Angebot werden die Releases auf OpenCoDE sein.

### AGPLv3

AGPLv3 ist weder für BundesMessenger noch für euch ein Problem. Es ändert sich
nichts für die Nutzung der bereitgestellten Gesamtlösung.

AGPLv3 ist genauso wie Apache 2.0 eine Open Source Lizenz und daher weiterhin
kostenfrei für uns alle nutzbar. Alle Firmen, die euch was anderes erzählen
haben es offensichtlich noch nicht verstanden. 😽

Wenn ihr den BundesMessenger so verwendet, wie wir ihn hier zur Verfügung
stellen, kann der Fall für euch nicht eintreten.

Alle Details zu AGPLv3 findet ihr
[hier](https://www.tldrlegal.com/license/gnu-affero-general-public-license-v3-agpl-3-0)

# Features & Vorteile

Es gibt eine [Aufstellung der Funktionen](features.md). Daneben gibt es eine
[Übersicht](warum-bum.md) der Vorteile des BundesMessengers.

# Roadmap

Wir haben uns auch für 2024 einiges vorgenommen. Hier findet ihr die großen
Punkte, die uns in der nächsten Zeit beschäftigen werden. Diese Aktivitäten
starten wir, schließen diese nicht unbedingt in 2024 ab.

- Föderation
- BuM X
- UTD
- MLS
- Huddle
- Herstellung der
  **[BITV](https://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html)
  Konformität** für alle Clients

Wir freuen uns über eure Anregungen und euer Feedback.

# Guidelines

BundesMessenger ist eine Gesamtlösung und wird als ein Produkt auf seine
Funktionalitäten und Fehler getestet. Nur so ist ein zuverlässiger und
zukünftiger Betrieb möglich.

Es sind folgende Core Module einzusetzen:

- Mobile Apps aus den Stores:
  - Android
  - iOS
- Backend:
  - Synapse
  - Sygnal
  - Redis
  - Matrix-Content-Scanner
  - BundesMessenger Web Client

Ausgangsbasis für die Installation des Backends ist immer das
[BundesMessenger Helm Chart](https://gitlab.opencode.de/bwi/bundesmessenger/backend/helm-chart)
und die zugehörigen BWI Container Images aus OpenCoDE. Ein Deployment muss in
einem DVS konformen Kubernetes Cluster erfolgen.

Folgende Services müssen außerhalb des BundesMessenger Namespaces bereitgestellt
werden:

- Datenbank
- Identity & Access Management
- Monitoring & Logging
- Externe Services, die über REST APIs angebunden werden z.B. Bots

Zur Verdeutlichung hier die Abbildung:

<!-- markdownlint-disable -->
<div align="center">
  <img src="images/bum_core_scope.png" alt="BundesMessenger Core Scope" width="600">
</div>
<!-- markdownlint-enable -->

Wir haben immer Interesse daran die Gesamtlösung oder einzelne Module zu
verbessern. Dafür benötigen wir euer Feedback.

Wenn Ihr eine Erweiterung habt, die auch für andere interessant sein könnte,
[kontaktiert uns](#bugs--feature-requests). Wenn Ihr Fehler oder Verbesserung
findet, [kontaktiert uns auch](#bugs--feature-requests).

Die Vision ist es auf Basis von Matrix Federations ein sicheres Netzwerk
(GovNet) zwischen den einzelnen BundesMessenger Nutzerhäusern zu etablieren. In
diesem sind **Sicherheit** und **Vertrauen** oberste Maxime.

Der Betrieb kann auch durch einen Dienstleister erbracht werden und muss nicht
durch Nutzerhaus sichergestellt werden. Dieser muss die genannten Anforderungen
erfüllen. Der [Antrag](https://messenger.bwi.de/ich-will-bum) zur Nutzung der
App kann **ausschließlich** durch das Nutzerhaus aus der öffentlichen Verwaltung
gestellt werden.

# Mitarbeit

Für die aktive Beteiligung müssen noch Grundlagen in der Zusammenarbeit
geschaffen werden. Wir befinden dazu gerade aktiv im Austausch und halten euch
hier auf dem laufenden. Ihr seid eingeladen dem Austausch im
[o.g. Raum](#ihr-wollt-einen-eigenen-messenger) beizutreten.

# Dokumentation

## Learning Portal

Unsere beiden Learning Portale für BundesMessenger sind hier online:

- für Endnutzer:innen ->
  [docs.bundesmessenger.info](https://docs.bundesmessenger.info)
- für Admin:innen ->
  [docs.bundesmessenger.dev](https://docs.bundesmessenger.dev)

## Technische Doku

Die technische Dokumentation ist in den zugehörigen Repos veröffentlicht:

- [Backend](https://gitlab.opencode.de/bwi/bundesmessenger/backend)
- [Clients](https://gitlab.opencode.de/bwi/bundesmessenger/clients)

Ein Learning Portal für Nutzer UND Admins arbeiten wir gerade und werden dieses
im Laufe dieses Jahres veröffentlichen.

# FAQ

Hier geht es zu den [FAQs](FAQ.md) weitere Antworten findest Du auch auf der
[BWI Projektseite](https://messenger.bwi.de/bundesmessenger).

# Support

Der Support für den BundesMessenger findet nur über die
[Community](#ihr-wollt-einen-eigenen-messenger) statt. Da jede Umgebung
individuell ist, sollte jeder für sich selbst die notwendigen Maßnahmen
ergreifen, die auch für die Einführung von anderer Software genutzt wird, um
BundesMessenger stabil und sicher zu betreiben.

Nutzt die **Community**, um euch über Fragestellungen zum Betrieb,
Infrastruktur, Fehlern etc. untereinander auszutauschen.

Fehler dürfen gerne über GitLab Issues gemeldet werden. Wir prüfen diese auf
Schwere, Dringlichkeit und Relevanz. Im Positivfall werden diese ins
Entwicklungsbacklog aufgenommen und in BundesMessenger realisiert.

# Bugs & Feature Requests

Bitte eröffnet dazu Issues in den jeweiligen OpenCoDE Projekten. Alternativ
könnt ihr euch im [BuM Community Raum](#ihr-wollt-einen-eigenen-messenger) auf
Matrix austauschen.

[Email](mailto:bundesmessenger@bwi.de) geht auch :wink:

# Rechtliches

## Copyright

- BWI GmbH [bwi.de](https://bwi.de/)
- Matrix Projekt [matrix.org](https://matrix.org/)
- Element.io [element.io](https://element.io/)

TL;DR: Wenn du es bis hier unten geschafft hast, sei so nett und gib unserem
Projekt einen 🌟 im GitLab (findest du ganz oben rechts auf dieser Seite).
