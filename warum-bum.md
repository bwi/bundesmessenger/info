# Welche Vorteile bietet Dir der Einsatz von BundesMessenger?

Die [BWI GmbH](https://www.bwi.de) hat Element schon für den Einsatz in der
Bundeswehr angepasst und folgende Funktionen und Prozesse eingeführt:

## Permanente Pflege

- Regelmäßige Updates aller Komponenten (Apps & Backend) alle 4 Wochen
- Kurzfristige Bereitstellung von Sicherheitsfixes

## Erhöhte Sicherheit für die mobilen Clients

- Erzwungener Schutz der App mit einer PIN
- Verifikation des Accounts durch ein zusätzliches Passwort für die Ende-zu-Ende
  Verschlüsselung
- Alle Inhalte verbleiben in der App. z.B. Bilder aus BuM können nicht mit
  anderen Apps geteilt werden, Bilder aus der Galerie können nicht mit BuM
  geteilt werden
- Integrierter Dateiviewer für PDF und TXT Dokumente
- Integrierte Kamera
- Kein Zugriff auf das Geräte-Telefonbuch erforderlich

## Permanente Ende-zu-Ende-Verschlüsselung in Chats

- Alle Chats sind ohne Ausnahme Ende-zu-Ende verschlüsselt

## Content Scanner

- Konfiguration von zugelassenen Medientypen, die als Anhang im Chat verschickt
  werden können
- Integration eines Anti-Viren Scanner für Anhänge

## Sichere Benachrichtigungen

- Sensible Informationen, wie z.B. Namen und Inhalte, werden nicht über die Push
  Server (von Google und Apple) übertragen und auch nicht in den
  Benachrichtigungen angezeigt

## Nutzung für die ÖV

- Verfügbar nur für den begrenzten Nutzerkreis der Öffentlichen Verwaltung
  (Bund, Land, Kommune)

### Governance durch die BWI

- Verifikation von teilnehmenden Nutzerhäusern aus der ÖV
- Bildung eines übergreifenden sicheren Kommunikationsverbunds
- [Registrierung](https://messenger.bwi.de/ich-will-bum) notwendig

## Digital souveräner Betrieb

- DVS
  ([Deutsche Verwaltungscloud-Strategie](https://www.cio.bund.de/Webs/CIO/DE/digitale-loesungen/digitale-souveraenitaet/deutsche-verwaltungscloud-strategie/deutsche-verwaltungscloud-strategie-node.html))
  konformes Container Deployment auf Basis Kubernetes
- Hoheit über die eigenen Daten
- Teilnahme an einer sicheren ÖV (Öffentlichen Verwaltung) Föderation (GovNet)

## Nutzung von nicht dienstlicher Hardware

- Die Apps sind in den App Stores von Android und iOS verfügbar
- Natürlich können diese auch auf dienstlichen Endgeräten über ein MDM
  (Mobile-Device-Management) verteilt werden

## Gemacht für Nutzer in der Öffentlichen Verwaltung

- Vereinfachte Bedienung
