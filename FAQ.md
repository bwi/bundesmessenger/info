# FAQ

- [Infrastruktur](#infrastruktur)
- [Apps](#apps)

## Infrastruktur

1. **Gibt es ein Architektur-Bild mit allen Infrastrukturkomponenten?**

   Es wird in Zukunft noch ein weiteres Repository im OpenCoDE geben. Dort
   werden neben den Core Modulen und Deployment-Skripten auch zusätzlich die
   notwendigen Referenz-Abbildungen zur Verfügung gestellt.

2. **Welche Erfahrungswerte zum Sizing bzw. den Bedarf an Infrastruktur gibt
   es?**

   Der Bedarf an Resourcen hängt maßgeblich von der Anzahl der Nutzer und deren
   Nutzungsverhalten ab. Valide Parameter für ein Kubernetes-Deployment gilt es
   für die Zukunft noch zu evaluieren. Diese sollen im Rahmen der Beta Phase
   ermittelt werden und als Referenz dienen.

   Wenn wir hierzu neue Erkenntnisse haben, teilen wir diese hier mit.

3. **Wie sieht eine BundesMessenger-Infrastruktur aus?**

   Die BundesMessenger Infrastruktur ist ein Cloudworkload basierend auf
   Erfahrungen des Secure Messaging Betriebs für die Bundeswehr.

   Die Lauffähigkeit wird auf einem Kubernetes Cluster auf Basis von
   [DVS](https://www.it-planungsrat.de/fileadmin/beschluesse/2021/Beschluss2021-46_Deutsche_Verwaltungscloud-Strategie_AL1.pdf)
   sichergestellt.

   Die Gesamtlösung setzt sich aus vielen Einzelkomponenten zusammen. Dazu
   gehören u.a. Matrix Basis Komponenten, BundesMessenger Webclient, Admin
   Portal und Module für den IT Basis Betrieb wie z.B. Reverse Proxy und
   Datenbank.

4. **Wie wird geregelt, mit wem meine Instanz föderiert bzw. Nachrichten
   austauschen darf?**

   Das erste Release des BundesMessenger kümmert sich maßgeblich um die
   Verfügbarkeit und den Aufbau von dezentralen BundesMessenger Instanzen.

   Im 2. Projektabschnitt (ab 2023) geht es genau darum diese Instanzen sicher
   untereinander zu verbinden. Das heißt Föderation.

   Hierzu werden wir technische Vorgaben und entsprechende Module entwickeln,
   die von allen BundesMessenger Teilnehmern genutzt werden können.

   Das Regelwerk für die Föderation ist aktuell für die Verbindung von mehreren
   BundesMessenger Instanzen aus unserer Sicht nicht ausreichend. Darum haben
   wir uns als Ziel gesetzt ÖV-konforme Föderation zu definieren, zu entwickeln
   und zu etablieren.

5. **Wie und an welchen Themen kann ich mitarbeiten und unterstützen?**

   Die BWI GmbH stellt ihre Arbeitsergebnisse als Open Source im OpenCoDE zur
   Nachnutzung zur Verfügung.

   Aktuell gehen mit einer kleinen Gruppe in eine Erprobungsphase um Skalierung
   und Sicherheit auf den Prüfstand zu stellen. Wir hoffen die Gruppe Ende des
   Jahres abhängig von den Ergebnissen erweitern zu können.

   Bis dahin ist jeder eingeladen in der
   [Community](README.md#ihr-wollt-einen-eigenen-messenger) zu diskutieren,
   Fragen zu stellen und eigene Ideen und Vorstellungen einzubringen.

   Nach der Veröffentlichung der ersten Open Source Projekte freuen wir uns auf
   eure Beteilungen über Issues im OpenCoDE.

## Apps

1. **Warum wird der Screen zur Schlüsselsicherung (Wiederherstellungsschlüssel)
   standardmäßig übersprungen? (Parameter: `var skipKeyBackupStep = true`)**

   _Oder: Wieso kann ich nicht wie in der Community Edition (FOSS) meine
   Schlüssel auf meinem Gerät sichern bzw. exportieren?_

   Um Irritationen auf Nutzerseite zu vermeiden "erzwingen" wir die
   Schlüsselsicherung nach jedem Login. Damit ist sichergestellt, dass der
   Nutzer auf allen Geräten immer alle Nachrichten lesen kann. Der Download des
   Schlüssels als Datei wurde aus Sicherheitsgründen entfernt, da insbesondere
   in Szenarien mit privaten Endgeräten nicht vermieden werden kann, dass dieser
   verloren oder von einer anderen App ausgelesen wird.

2. **Warum werden Push Mitteilungen standardmäßig nicht entschlüsselt gepusht?
   (Parameter: `include_content: false`)**

   _Oder: Ich erhalte bei den Push-Benachrichtigungen nur die Information, dass
   eine Nachricht vorliegt, sehe aber nicht deren Inhalt._

   Der BundesMessenger wird mit dem Fokus "Security by Design" entwickelt. Daher
   werden alle Push Mitteilungen ohne Nachrichteninhalt nach die Endgeräte
   versendet. D.h., dass vom Server über die Push-Provider (Google/Apple) nur
   die Information versendet wird, dass eine neue Nachricht vor liegt, jedoch
   nicht der Inhalt. Um den den Inhalt der neuen Nachrichten auf dem
   Sperrbildschirm anzeigen zu können, müssten die Inhalte via Push-Provider
   versendet werden. Auf die Weise werden bei gestohlenen Geräten keine sensible
   Informationen preisgeben und die Daten, die über die Push-Provider
   (US-Konzerne) geleitet werden, minimiert.

3. **Warum ist die Video Funktionalität per Standard deaktiviert?**

   _Oder: Warum kann ich keine Video-Gespräche mit dem BundesMessenger führen?_

   Der BundesMessenger ist von Haus aus auf "Security by Design" ausgelegt. Da
   die Jitsi Integration per Standard nicht auf Ende zu Ende Verschlüsselung
   ausgelegt ist, wurde dies deaktiviert. Aktuell wird eine Ende zu Ende
   verschlüsselte Video Lösung entwickelt (Huddle Meetings), die dieses Jahr für
   den Web Client veröffentlicht werden soll. Die mobilen Clients folgen dann im
   Jahr 2024.

4. **Ist es grundsätzlich möglich, diese Software auch in einem vollständig
   geschlossenen Netz mit VPN Architektur oder gar closed user group zu
   betreiben? Ich beziehe mich da beispielsweise auf die nicht mögliche
   Verwendung von selbst signierten Zertifikaten.**

   Ja, das ist möglich. Das Backend kann in einem geschlossenen Netz betrieben
   werden. Die Apps aus den Stores können über AppConfig und einem MDM auch mit
   einem app-spezifischen VPN konfiguriert werden, so dass diese dann nur
   innerhalb des konfigurierten Netzwerkes funktionieren. Es ist aber nicht
   möglich die Standard Apps aus den App Stores mit einem Zertfikat zu nutzen,
   welches nicht von einer offiziellen öffentlichen CA abgeleitet ist.

5. **Ist der BundesMessenger einem Pen Test unterzogen worden?**

   Wir führen regelmäßige Pen Tests mit dem BwMessenger durch und korrigieren
   die Findings zeitnah in unserem Entwicklungsintervall. Da der BwMessenger und
   der BundesMessenger die identische Code Basis besitzen, gilt dies daher auch
   für den BundesMessenger.

6. **Wie sieht es mit dem Thema Barrierefreiheit im BundesMessenger aus?**

   Der BwMessenger wurde hinsichtlich des BITV 2.0 Standards untersucht. Die
   ausgearbeiteten Verbesserungen werden aktuell von uns im BwMessenger
   integriert und landen daher auch im BundesMessenger. Die Fertigstellung der
   Integration soll bis Ende 2023 abgeschlossen sein.

7. **Wieso können Nachrichten nicht entschlüsselt werden, wenn ich nicht am Client
   (z.B. iOS oder Web App) angemeldet bin? (keine aktive Session)?**

   Eine aktive Session entsprichst einer Anmeldung auf einem Client BundesMessenger
   z.B. Web oder Android App. Durch eine Abmeldung am Client verliert man seine
   Session. Ist man also an keinem Client angemeldet, führt dies dazu, das Nachrichten
   nicht entschlüsselt werden können. Wir arbeiten aktuell aktiv daran, dass die
   sicheren Schlüssel zum entschlüsseln deiner Nachrichten auch dann vorgehalten werden,
   wenn du keine aktive Sitzung im BundesMessenger hast. Grundsätzlich empfehlen wir
   aber, sich in der mobilen App nicht abzumelden, sondern z.B. die Benachrichtigungszeiten
   einzustellen, sodass man außerhalb der Verfügbarkeiten keine Benachrichtigungen bekommt.

8. **Ich habe nicht entschlüsselbare Nachrichten in meinen Chats. Woran kann das liegen??**

   Das Problem läuft bei uns unter der Bezeichnung „Unable to decrypt“ (UTD) und ist auch in
   Element bekannt (https://github.com/element-hq/element-meta/issues/245 ). Leider gibt es
   für das Problem verschiedene Ursachen. Seit Q3/2023 arbeiten wir mit Element Software
   daran dieses Thema prioritär zu beheben. Bis dahin können wir folgende Maßnahmen empfehlen,
   um nicht entschlüsselbare Nachrichten zu vermeiden:
   - Leeren des Caches (iOS/Android)
   - Neuladen der Seite mit STRG+F5 (Webclient)
   - Nutzen der Funktion „Sichern und Wiederherstellung der Schlüsselsicherung“
   (über Einstellungen → Sicherheit & Privatsphäre → Erstellung & Wiederherstellung
   verschlüsselter Nachrichten → VON SICHERUNG WIEDERHERSTELLEN)
   - Ab- und Anmeldung am Messenger mittels Eingabe des Wiederherstellungsschlüssels
   oder mittels Verifikation über ein anderes Gerät
   - Nutzung auf mehreren Geräten und Plattformen, um korrekten Schlüsselaustausch sicherzustellen

   Falls alle vorherigen Maßnahmen keine Wirkung zeigen, können auch folgende Maßnahmen ergriffen
   werden (Achtung: Hierbei kann es zu Datenverlusten in den Chats kommen).

   - Räume verlassen und ggf. wieder neu betreten (ggf. Verlust der bisherigen Chats und
   Nichtzustellung der Nachrichten in Abwesenheit)
   - Direktchats verlassen und neu eröffnen (neuer 1:1-Raum wird generiert)

9. **Existieren Tracking Funktionalitäten im BundesMessenger?**

   Um diese Frage zu beantworten muss man kurz auf unseren Fertigungsprozess des BundesMessengers
   eingehen. Der BwMessenger entsteht bzw. leitet sich aus dem BundesMessenger ab. Daher ist
   der BundesMessenger ein Vorprodukt vom BwMessenger. Da wir im BwMessenger Matomo zur anonymisierten
   Auswertung verwenden, ist der Code hierfür auch im BundesMessenger enthalten. Allerdings ist
   dieser Code im BundesMessenger deaktiviert und kann auch nicht durch ein Nutzungshaus nachträglich
   aktiviert werden. Dies müsste durch unser Entwicklungsteam vorgenommen werden. Daher existiert kein
   Tracking im BundesMessenger, allerdings ist die Codebasis hierfür aus den vorher genannten Gründen
   vorhanden – aber deaktiviert. Gerne können dies auch im Open CoDE Repository so nachvollziehen, da
   dort der Code aller BundesMessenger Clients veröffentlicht ist.
